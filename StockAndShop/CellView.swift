//
//  CellView.swift
//  StockAndShop
//
//  Created by COTEMIG on 06/06/22.
//

import UIKit

class CellView: UICollectionViewCell {
    @IBOutlet weak var titleProduct: UILabel!
    @IBOutlet weak var imageProduct: UIImageView!
    @IBOutlet weak var priceProduct: UILabel!
    @IBOutlet weak var clientName: UILabel!
}
