//
//  ProductDetailsViewController.swift
//  StockAndShop
//
//  Created by COTEMIG on 13/06/22.
//

import UIKit

class ProductDetailsViewController: UIViewController {

    @IBOutlet weak var imageProduct: UIImageView!
    
    @IBOutlet weak var nameProduct: UILabel!
    var product : Product?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let product = product{
            imageProduct.image = product.image
            nameProduct.text = product.name
        }
    }

    @IBAction func clickBack(_ sender: Any) {
        dismiss(animated: true)
    }
}
