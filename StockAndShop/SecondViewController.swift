//
//  SecondViewController.swift
//  StockAndShop
//
//  Created by COTEMIG on 23/05/22.
//

struct Product {
    let name: String
    let price: Float
    let client: String
    let image: UIImage?
    let section: String
    let categories: Array<String>
    
}


import UIKit

class SecondViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet weak var CollectionProducts: UICollectionView!
    let arrayProducts: [Product] = [
        Product(name: "Sabonete", price: 5.00, client: "Sabrina", image: UIImage(named: "sabonete"), section: "cosmeticos", categories: ["Body","Facial"]),
        Product(name: "Sabonete2", price: 5.50, client: "Sabrina2", image: UIImage(named: "sabonete"), section: "cosmeticos", categories: ["Body","Facial"]),
        Product(name: "Sabonete3", price: 3.00, client: "Sabrina3", image: UIImage(named: "sabonete"), section: "cosmeticos", categories: ["Body","Facial"]),
        Product(name: "Sabonete4", price: 66.00, client: "Sabrina4", image: UIImage(named: "sabonete"), section: "cosmeticos", categories: ["Body","Facial"]),
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let itemSize = UIScreen.main.bounds.width/2 - 40
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: itemSize, height: 190   )

        layout.minimumInteritemSpacing = 3
        layout.minimumLineSpacing = 3

        CollectionProducts.collectionViewLayout = layout
        CollectionProducts.dataSource = self
        CollectionProducts.delegate = self
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayProducts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"Cell", for: indexPath) as! CellView
        
        let currentProduct = arrayProducts[indexPath.row]
        
        cell.titleProduct.text = currentProduct.name
        cell.imageProduct.image = currentProduct.image
        cell.clientName.text = currentProduct.client
        cell.priceProduct.text = "R$" + String(currentProduct.price)
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DetailProduct"{
            let productSelected = sender as! Product
            let viewController = segue.destination as! ProductDetailsViewController
            viewController.product = productSelected
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let product = arrayProducts[indexPath.row]
        performSegue(withIdentifier: "DetailProduct", sender: product)
    }
    
}
