//
//  ViewController.swift
//  StockAndShop
//
//  Created by COTEMIG on 23/05/22.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var createAccount: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: Selector("tapFunction:"))
        createAccount.addGestureRecognizer(tap)
    }
    
    func tapFunction(sender:UITapGestureRecognizer) {
        performSegue(withIdentifier: "goToProducts", sender: nil)
    }
    
    func showSuccessDialog() {
        presentAlert(title: "Conta criada!", message: "Sua conta foi criada e já é possível utiliza-la!") {
            // o dissmis será ação executada quando o botão de "OK" for clicado, você pode passar qualquer coisa aqui, teste utilizando um print e veja o que aconteça.
            self.dismiss(animated: true)
        }
    }

}


extension UIViewController {
    
    func presentAlert(title: String, message: String, actionHandler: (() -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default) { _ in
            actionHandler?()
        })
        present(alert, animated: true)
    }
}
